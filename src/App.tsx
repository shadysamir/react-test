import './App.css'

function App() {

  return (
    <div className="app">
      <form className="main-form">
        <div className="select-container">
          <label>
            Country:
            <select title="Country" name="country">
            </select>
          </label>
        </div>
        <div className="select-container">
          <label>
            State:
            <select title="State" name="state">
            </select>
          </label>
        </div>
        <div className="select-container">
          <label>
            City:
            <select title="City" name="city">
            </select>
          </label>
        </div>
      </form>
    </div>
  )
}

export default App
