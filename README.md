# React test task

This project is the starting point for a task to test developer React skills. The task is to achieve this UI based on an API call to `https://storage.googleapis.com/bringy-staging-public/fake-api/data.json`. A sample of the response of this API is in `data.json` file located at the root of this project.

![react-task-dropdown-larger](https://gitlab.com/shadysamir/react-test/-/wikis/uploads/36d8cf0f5fc6f7b2b19ee9bb40e259f1/react-task-dropdown-larger.gif)

## Preparation

- Clone the repo to your computer
- If you are going to solve the task using TypeScript then use `main` branch. If you want to use JavaScript instead, then checkout `js` branch.
- Run `npm install` at the root of the project.

## Testing your code

The application uses Vite to build and run code. To run and watch your code use `npm run dev`.
